const express = require('express');
const router = express.Router();
const userController = require('../controllers/transaction')();


router.post('/', userController.transaction);

module.exports = router;
