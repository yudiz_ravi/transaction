const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    sUsername: {
		type: String,
        required: [true, 'Please add a username...'],
	},
    nBalance: {
        type: Number,
        minLength: 0,
        maxLength: 1000,
        trim: true,
        match: [
            /^\d{0,1000}$/,
            'Please add a valid Balance...',
        ],
        required: [true, 'Please add a Balance...'],
        default: 0
    }
}, {timestamps: true });

const user = mongoose.model('user', userSchema);
module.exports = user;