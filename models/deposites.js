const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const depositeSchema = new Schema({
  iUserId: {
		type: mongoose.Schema.Types.ObjectId, 
    ref: 'users', 
    required: [true, 'Please add a user id...'],
	},
  nAmount: {
		type: Number,
    required: [true, 'Please add a deposite Amount...'],
	},
}, {timestamps: true });

const deposite = mongoose.model('deposite', depositeSchema);
module.exports = deposite;