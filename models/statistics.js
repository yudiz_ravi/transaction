const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const statisticSchema = new Schema({
    // iUserId
	// nTotalDeposit ( total deposited balance by user)
	// nDepositsCount
    iUserId: {
		type: mongoose.Schema.Types.ObjectId, 
        ref: 'users', 
        required: [true, 'Please add a user id...'],
	},
    nTotalDeposit: {
        type: Number,
        minLength: 0,
        maxLength: 1000,
        trim: true,
        match: [
            /^\d{0,1000}$/,
            'Please add a valid Balance...',
        ],
        required: [true, 'Please add a Balance...'],
    },
    nDepositsCount: {
        type: Number,
        required: true
    }
}, {timestamps: true });

const statistic = mongoose.model('statistic', statisticSchema);
module.exports = statistic;