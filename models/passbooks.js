const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const passbookSchema = new Schema({
    iUserId: {
		type: mongoose.Schema.Types.ObjectId, 
        ref: 'users', 
        required: [true, 'Please add a user id...'],
	},
    iTransactionId: { 
        type: mongoose.Schema.Types.ObjectId,  
        ref: 'deposites', 
        required: [true, 'Please add a transaction id...']
    },
    nPrevBalance: {
        type: Number,
        minLength: 0,
        maxLength: 1000,
        trim: true,
        match: [
            /^\d{0,1000}$/,
            'Please add a valid Balance...',
        ],
        // required: [true, 'Please add a Balance...'],
    },
    nAmount: {
		type: Number,
        required: [true, 'Please add a deposite Amount...'],
	},
    nBalance: {
        type: Number,
        minLength: 0,
        maxLength: 1000,
        trim: true,
        match: [
            /^\d{0,1000}$/,
            'Please add a valid Balance...',
        ],
        required: [true, 'Please add a Balance...'],
    },
    
}, {timestamps: true });

const passbook = mongoose.model('passbook', passbookSchema);
module.exports = passbook;