const express = require('express');
const app = express();
const logger = require('./config/logger');
const usersRouter = require('./routes/users');

const connectDB = require('./config/db');
connectDB();

app.use(express.json());
app.use(logger());

app.use('/users', usersRouter);

const PORT = process.env.PORT || 3000;
const server = require('http').createServer(app);
server.listen(PORT, ()=> { console.log(`Server running on port ${PORT}...`) });