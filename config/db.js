const mongoose = require('mongoose');
const MONGO_URI = process.env.MONGO_URI || "mongodb+srv://*****:******@cluster0.m9kfz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
'mongodb://localhost:27017/test?retryWrites=true&w=majority';


const connectDB = async () => {
	try {
		const conn = await mongoose.connect(MONGO_URI, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			serverSelectionTimeoutMS: 5000,
			useFindAndModify: false,
			// poolSize: 20
		});
		
		console.log(`MongoDB Connected: ${conn.connection.host}`);
	} catch (err) {
		console.error('MongoDB Connection Error: ' + err);
		process.exit(1);    // Uncaught Fatal Exception...
	}
};

module.exports = connectDB;