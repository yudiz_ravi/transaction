const morgan = require('morgan');
const fs = require('fs');
const path = require('path');

const accessLogStream = fs.createWriteStream(path.join(__dirname, '../access.log'), { flags: 'a' })

module.exports =  function logger() {
    return morgan(function (tokens, req, res) {
        return [
          tokens.method(req, res),
          tokens.url(req, res), '-', 
          tokens['remote-addr'](req, res), '-', 
          tokens.status(req, res),
          tokens.res(req, res, 'content-length'), '-',
          tokens['response-time'](req, res), 'ms'
        ].join(' ')
      },
      { 
        stream: accessLogStream,
        skip: function (req, res) { return res.statusCode < 400 } 
      }
    );
}
// setup the logger
// app.use(morgan('combined', { 
//     stream: accessLogStream,
//     skip: function (req, res) { return res.statusCode < 400 } 
// }));