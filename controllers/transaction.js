const mongoose = require('mongoose');
const users = require('../models/users');
const deposites = require('../models/deposites');
const passbooks = require('../models/passbooks');
const statistics = require('../models/statistics');

module.exports = function controller(req, res) {
    return {
        transaction: async(req, res)=> {
            
          const session = await mongoose.startSession();
          const transactionOptions = {
            readPreference: 'primary',
            readConcern: { level: 'local' },
            writeConcern: { w: 'majority' }
          };
        
          try {
                await session.withTransaction(async () => {
                const { iUserId, nAmount } = req.body;

                const balance = await users.findById(iUserId).select({nBalance: 1});
                const newBalance = balance.nBalance + nAmount;

                const deposite = new deposites({
                    iUserId: iUserId,
                    nAmount: nAmount
                });
                // console.log(`deposite :: `, deposite)
                const trans1 = await deposites.create([deposite],  { session });
                console.log(`trans1 :: `, trans1);
                const updateContent = {
                    $set: {
                        nBalance: newBalance
                    }
                };
                // console.log(`updateContent ::`, updateContent);
                const trans2 = await users.findByIdAndUpdate(iUserId, updateContent, { session });
                console.log(`trans2 :: `, trans2);
            
                const passbook = new passbooks({
                        iUserId,
                        iTransactionId: trans1[0]._id,
                        nPrevBalance: balance.nBalance,
                        nAmount,
                        nBalance: newBalance
                    });
                // console.log(`passbook ::`, passbook);
                const trans3 = await passbooks.create([passbook], { session });
                console.log(`trans3 :: `, trans3);
                
                const statistic = {
                    $set: { 'nTotalDeposit': newBalance }, 
                    $inc: {
                        'nDepositsCount': 1
                    }
                };

                // console.log(`statistics ::`, statistic);
                const trans4 = await statistics.findOneAndUpdate(iUserId, statistic, { session });
                console.log(`trans4 :: `, trans4);
                await session.commitTransaction();
                return res.status(201).json({
                    'status': true,
                    'code': 201,
                    'message': 'You Successfully Deposited Money....',
                });
            }, transactionOptions);
          
            } catch (error) {
                console.log('Transaction aborted. Caught exception during transaction.');
                console.log(error);
                await session.abortTransaction();
            } finally {
                console.log('Transaction committed.');
                await session.endSession();
            }
        
        },
    }
}